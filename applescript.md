# Applescripts

## Get parent folder path
```
on run {input, parameters}
	tell application "Finder"
		set pwdAlias to insertion location as alias
		if not (exists folder pwdAlias) then
			set pwdAlias to (container of pwdAlias) as alias
		end if
	end tell
	set input to POSIX path of pwdAlias
	display alert input
	return input
end run
```

## Run Terminal command and display its output
```
on run {input, parameters}
	set commandpiece to "wc -l " as text
	set address to POSIX path of input
	set command to (commandpiece & quoted form of address) as text
	--display dialog command
	set exitCode to do shell script command
	--display dialog exitCode
	set AppleScript's text item delimiters to " "
	set textParts to (every text item in exitCode) as list
	set rowcount to item 8 of textParts
	set filelink to item 9 of textParts
	set AppleScript's text item delimiters to ""
	
	display dialog ("Row count: " & rowcount & "\n" & filelink ) as text
	return exitCode as text

end run
```

## Remove characters from string:
[source](http://applehelpwriter.com/2016/09/06/applescript-remove-characters-from-a-string/)   
Here’s the declarations you’ll need:   
```
use scripting additions
use framework "Foundation"
property NSString : a reference to current application's NSString
```   
Here’s the code for the handler:   
```
on remove:remove_string fromString:source_string	
set s_String to NSString's stringWithString:source_string
set r_String to NSString's stringWithString:remove_string
return s_String's stringByReplacingOccurrencesOfString:r_String withString:""
end remove:fromString:
```   
