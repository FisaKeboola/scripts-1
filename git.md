# bitbucket

## bitbucket-cli.

Install it using pip
```
pip3 install bitbucket-cli
```
Then create a repo using
```
bitbucket create --private --protocol ssh --scm git YOUR_REPO_NAME
```
Note that this creates a private git repo, you can use --public for public access and --scm hg if you use Mercurial. Username argument can be added via --username YOUR_USER_NAME.

### Fix error message: "Git: fatal: The current branch master has multiple upstream branches, refusing to push"
```
git config remote.origin.push HEAD
```